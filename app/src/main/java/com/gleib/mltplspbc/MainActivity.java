package com.gleib.mltplspbc;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class MainActivity
        extends AppCompatActivity
        implements  OnListInteractionListener,
                    CommandResultReceiver.ResultReceiverCallback<String> {

    static final Pattern IP_ADDRESS_PATTERN = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    List<Device> mDevices;
    DevicesRecyclerViewAdapter mAdapter;
    CommandResultReceiver mCommandResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mCommandResultReceiver = new CommandResultReceiver(new Handler());
        this.mCommandResultReceiver.setReceiver(this);

        List<Device> x;
        try {
            x = Device.loadList(openFileInput("device_list"));
        } catch (FileNotFoundException e) {
            x = Device.newList();
        }
        this.mDevices = x;
        this.mAdapter = new DevicesRecyclerViewAdapter(this.mDevices, this, this.mCommandResultReceiver);

        System.out.printf("devices list size: %d%n", this.mDevices.size());

        RecyclerView rv = findViewById(R.id.recycler_view_devices);
        rv.setAdapter(this.mAdapter);
        rv.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));

        View l = findViewById(R.id.activity_main_layout);
        l.setOnLongClickListener(v -> {
            this.newDevice();
            return true;
        });
    }

    public void newDevice() {

        final Device item = new Device("NAME", "127.0.0.1");
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.device_data_dialog);
        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;

        final EditText txtName = dialog.findViewById(R.id.device_data_dialog_txtName);
        final EditText txtAddress = dialog.findViewById(R.id.device_data_dialog_txtAddress);
        final Button btnOK = dialog.findViewById(R.id.device_data_dialog_cmdOK);
        final Button btnCancel = dialog.findViewById(R.id.device_data_dialog_cmdCancel);
        final ImageButton btnDelete = dialog.findViewById(R.id.device_data_dialog_btnDelete);

        txtAddress.setText(item.address);
        txtName.setText(item.name);

        txtAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (IP_ADDRESS_PATTERN.matcher(s.toString()).matches()) {
                    btnOK.setEnabled(true);
                } else {
                    btnOK.setEnabled(false);
                    txtAddress.setError("INVALID ADDRESS");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        btnOK.setOnClickListener(view -> {

            item.address = txtAddress.getText().toString();
            item.name = txtName.getText().toString();

            mDevices.add(item);
            mAdapter.notifyItemInserted(mDevices.size());

            try {
                Device.saveList(openFileOutput("device_list", Context.MODE_PRIVATE), mDevices);
            } catch (FileNotFoundException e) {
            }

            dialog.dismiss();
        });

        btnDelete.setOnClickListener(v -> {

            dialog.dismiss();
        });

        btnCancel.setOnClickListener(view -> {

            dialog.dismiss();
        });

        dialog.show();
    }

    @Override
    public void onListInteraction(Device item, Integer position) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.device_data_dialog);
        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;

        final EditText txtName = dialog.findViewById(R.id.device_data_dialog_txtName);
        final EditText txtAddress = dialog.findViewById(R.id.device_data_dialog_txtAddress);
        final Button btnOK = dialog.findViewById(R.id.device_data_dialog_cmdOK);
        final Button btnCancel = dialog.findViewById(R.id.device_data_dialog_cmdCancel);
        final ImageButton btnDelete = dialog.findViewById(R.id.device_data_dialog_btnDelete);

        txtAddress.setText(item.address);
        txtName.setText(item.name);

        txtAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (IP_ADDRESS_PATTERN.matcher(s.toString()).matches()) {
                    btnOK.setEnabled(true);
                } else {
                    btnOK.setEnabled(false);
                    txtAddress.setError("INVALID ADDRESS");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        btnOK.setOnClickListener(v -> {

            item.address = txtAddress.getText().toString();
            item.name = txtName.getText().toString();

            mAdapter.notifyItemChanged(position);

            try {
                Device.saveList(openFileOutput("device_list", Context.MODE_PRIVATE), mDevices);
            } catch (FileNotFoundException e) {
            }

            dialog.dismiss();
        });

        btnDelete.setOnClickListener(v -> {

            Iterator<Device> i = mDevices.iterator();
            boolean x = false;
            int c = -1;
            do {
                c++;
                Device dev = i.next();
                if (dev.name.trim().equals(item.name.trim())
                        && dev.address.trim().equals(item.address.trim())) {
                    x = true;
                    i.remove();
                }
            } while (i.hasNext() && !x);

            try {
                Device.saveList(openFileOutput("device_list", Context.MODE_PRIVATE), mDevices);
            } catch (FileNotFoundException e) {
            }

            mAdapter.notifyItemRemoved(c);
            dialog.dismiss();
        });

        btnCancel.setOnClickListener(v -> {

            dialog.dismiss();
        });

        dialog.show();
    }

    @Override
    public void onCommandSuccess(String data) {

        if (data.equals("{\"system\":{\"set_relay_state\":{\"err_code\":0}}}")) {
            data = "OK";
        }
        Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCommandError(Exception exception) {
        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}
