package com.gleib.mltplspbc;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by gleib on 13/01/2018.
 */

public class DevicesRecyclerViewAdapter extends RecyclerView.Adapter<DevicesRecyclerViewAdapter.ViewHolder> {

    private List<Device> mDataset;
    private OnListInteractionListener mListener;
    private ResultReceiver mResultReceiver;

    public DevicesRecyclerViewAdapter(List<Device> mDataset, OnListInteractionListener mListener, ResultReceiver mResultReceiver) {
        this.mDataset = mDataset;
        this.mListener = mListener;
        this.mResultReceiver = mResultReceiver;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.device_list_element, parent, false);

        final ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Device item = this.mDataset.get(position);
        holder.mItem = item;
        holder.tvAddress.setText(item.address);
        holder.tvName.setText(item.name);

        holder.btnON.setOnClickListener(v -> {

            System.out.printf("COMMAND %s ON%n", item.name);

            Intent i = new Intent(v.getContext(), CommandIntentService.class);
            Bundle b = new Bundle();
            b.putSerializable("DEVICE", item);
            b.putString("COMMAND", Device.CMD_ON);
            i.putExtra("EXTRABUNDLE", b);
            i.putExtra(CommandResultReceiver.NAME, this.mResultReceiver);
            v.getContext().startService(i);
        });

        holder.btnOFF.setOnClickListener(v -> {

            System.out.printf("COMMAND %s OFF%n", item.name);

            Intent i = new Intent(v.getContext(), CommandIntentService.class);
            Bundle b = new Bundle();
            b.putSerializable("DEVICE", item);
            b.putString("COMMAND", Device.CMD_OFF);
            i.putExtra("EXTRABUNDLE", b);
            i.putExtra(CommandResultReceiver.NAME, this.mResultReceiver);
            v.getContext().startService(i);
        });

        holder.cardView.setOnLongClickListener(v -> {
            System.out.printf("position: %d%n", position);
            mListener.onListInteraction(holder.mItem, position);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return this.mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public TextView tvName;
        public TextView tvAddress;
        public Button btnON;
        public Button btnOFF;
        public Device mItem;
        public CardView cardView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            this.tvAddress = view.findViewById(R.id.tvAddress);
            this.tvName = view.findViewById(R.id.tvName);
            this.btnOFF = view.findViewById(R.id.btnOFF);
            this.btnON = view.findViewById(R.id.btnON);
            this.cardView = view.findViewById(R.id.cardview_device);
        }

    }
}
