package com.gleib.mltplspbc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by gleib on 13/01/2018.
 */

public class Device implements Serializable {

    public final static String CMD_INFO = "{\"system\":{\"get_sysinfo\":{}}}";
    public final static String CMD_ON = "{\"system\":{\"set_relay_state\":{\"state\":1}}}";
    public final static String CMD_OFF = "{\"system\":{\"set_relay_state\":{\"state\":0}}}";

    public String name;
    public String address;

    private Device() {
    }

    public Device(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public static List<Device> loadList(FileInputStream fis) {

        List<Device> ret = new LinkedList<>();

        try {
            ObjectInputStream ois = new ObjectInputStream(fis);
            ret = (List<Device>) ois.readObject();
            ois.close();
            fis.close();
            System.out.printf("list size: %s%n", ret.size());
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
        }

        return ret;
    }

    public static void saveList(FileOutputStream fos, List<Device> list) {

        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
            oos.close();
            fos.close();
        } catch (IOException e) {
        }
    }

    public static List<Device> newList() {

        List<Device> ret = new LinkedList<>();

        ret.add(new Device("EDIT ME", "127.0.0.1"));
        return ret;
    }

    public static byte[] encrypt(String str) {

        byte[] ret = new byte[str.length() + 4];
        ret[0] = 0;
        ret[1] = 0;
        ret[2] = 0;
        ret[3] = 0;

        byte key = (byte) 171;
        for (int i = 0; i < str.length(); i++) {

            byte a = (byte) (key ^ str.charAt(i));
            key = a;
            ret[i + 4] = a;
        }

        return ret;
    }

    public static String decrypt(byte[] data) {

        StringBuilder sb = new StringBuilder();
        byte key = (byte) 171;
        for (int i = 4; i < data.length; i++) {

            byte a = (byte) (key ^ data[i]);
            key = data[i];
            sb.append((char) a);
        }

        return sb.toString();
    }

    //<editor-fold desc=" Overrides ">
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device = (Device) o;

        return name.equals(device.name) && address.equals(device.address);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }
    //</editor-fold>

}
