package com.gleib.mltplspbc;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;


/**
 * Created by gleib on 14/01/2018.
 */

public class CommandIntentService extends IntentService {

    public CommandIntentService() {
        super("CommandIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Bundle b = intent.getBundleExtra("EXTRABUNDLE");
        Device dev = (Device) b.getSerializable("DEVICE");
        String cmd = b.getString("COMMAND");

        System.out.printf("SENDING TO %s - %s%n", dev.name, dev.address);

        ResultReceiver rr = intent.getParcelableExtra(CommandResultReceiver.NAME);

        Socket socket;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(dev.address, 9999), 5000);

            OutputStream os = socket.getOutputStream();
            os.write(Device.encrypt(cmd));

            byte[] buf = new byte[8192];
            InputStream is = socket.getInputStream();

            int r = is.read(buf);

            r = (r < 0) ? 0 : r;
            if (r > 0) {

                byte[] x = new byte[r];
                System.arraycopy(buf, 0, x, 0, r);

                String res = Device.decrypt(x);
                System.out.printf("DATA RETURNED: %s%n", res);

                b = new Bundle();
                b.putSerializable(CommandResultReceiver.PARAM_RESULT, res);

                rr.send(CommandResultReceiver.RESULT_CODE_OK, b);
            } else {
                System.out.printf("DEVICE RETURNED NOTHING");

                b = new Bundle();
                b.putSerializable(CommandResultReceiver.PARAM_RESULT, "DEVICE RETURNED NOTHING");

                rr.send(CommandResultReceiver.RESULT_CODE_ERROR, b);
            }
        } catch (IOException e) {

            b = new Bundle();
            b.putSerializable(CommandResultReceiver.PARAM_EXCEPTION, e);

            rr.send(CommandResultReceiver.RESULT_CODE_ERROR, b);
        }

    }
}
